//import './App.css';
import ExampleBasic from "./Components/ExampleBasic.js";
import Example1 from "./Components/Example1.js";
import Example2 from "./Components/Example2.js";
import Example3 from "./Components/Example3.js";
import Example4 from "./Components/Example4.js";
import Example5 from "./Components/Example5.js";
import Example6 from "./Components/Example6.js";
import Example7 from "./Components/Example7.js";
import Like from "./Components/Like.js";
import Stars from "./Components/Star.js";
import {useState} from "react";

function App() {
    const [checked, setChecked] = useState(false);
    const [star, setStar] = useState(0);

        return (
        <div className="App">
            <Like checked={checked} onChange={setChecked}/>
            <hr/>
            <Stars rating={star} onChange={setStar}/>
            <hr/>
            <hr/>
            <hr/>
            <ExampleBasic/>
            <hr/>
            <Example1/>
            <hr/>
            <Example2/>
            <hr/>
            <Example3/>
            <hr/>
            <Example4/>
            <hr/>
            <h1>Afternoon</h1>
            <hr/>
            <Example5/>
            <hr/>
            <Example6/>
            <hr/>
            <Example7/>
            <hr/>
        </div>
    );
}

export default App;
