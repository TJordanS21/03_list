export default function ExampleBasic() {

    return (
        <div>
            <h1>Basic Example</h1>
            <ul>
                <li>red</li>
                <li>green</li>
                <li>blue</li>
            </ul>
        </div>
    )
}