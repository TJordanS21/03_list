export default function Example2() {
    const data = [
        {title: 'red', red: 255, green: 0, blue: 0},
        {title: 'green', red: 0, green: 255, blue: 0},
        {title: 'blue', red: 0, green: 0, blue: 255}
    ]

    return (
        <div>
            <h1>Example 2</h1>
            <ul>{data.map((color, index) =>
                <li>{color.title + " (" + color.red + "," + color.green + "," + color.blue + ")"}</li>)}
            </ul>
        </div>
    )
}