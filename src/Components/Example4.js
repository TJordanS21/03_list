import React, {useState} from 'react';

export default function Example4() {
    const [data, setData] = useState([]);
    const [text, setText] = useState('');

    function handleSubmit(event) {
        const getRandomValue = () => {
            const {floor, random} = Math;
            return floor(random() * 255);
        }

        setData([...data, {title: text, red: getRandomValue(), green: getRandomValue(), blue: getRandomValue()}]);
    }

    return (
        <div>
            <h1>Example 4</h1>
            <ul>{data.map(({title, red, green, blue}, index) =>
                <li style={{color: `rgb(${red},${green},${blue})`}}>{title + " (" + red + "," + green + "," + blue + ")"}</li>)}
            </ul>
            <h3>Add a color!!!</h3>
            <input type="text" value={text} onChange={(e) => setText(e.target.value)}/>
            <input type="button" value="Submit" onClick={handleSubmit}/>
        </div>
    )
}